#!/usr/bin/env sh

BIN_NAME="${1:-secrets-replicator}"

APP_COMMIT="$(git rev-parse --short=7 HEAD)"
APP_VERSION="$(git describe --tags --dirty --abbrev=7 2>/dev/null)"

CGO_ENABLED=0 go build -o "${BIN_NAME}" -ldflags "-s -w -X main.AppVersion=${APP_VERSION:-${APP_COMMIT}} -X main.AppCommit=${APP_COMMIT}"
upx -6 "${BIN_NAME}"
