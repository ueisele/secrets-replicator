package main

import "time"

type Config struct {
	Kubeconfig       string
	Timeout          time.Duration
	Namespaces       []string
	ResyncPeriod     time.Duration
	AnnotationPrefix string
	StatusListener   string
	LogLevel         string
	LogFormat        string
}
