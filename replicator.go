package main

import (
	"context"
	"fmt"
	"github.com/pkg/errors"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/client-go/informers"
	"k8s.io/client-go/kubernetes"
	listers "k8s.io/client-go/listers/core/v1"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/util/homedir"
	"k8s.io/client-go/util/workqueue"
	"os"
	"path/filepath"
	"secrets-replicator/pkg/signals"
	"strings"
	"sync"
	"time"
)

const (
	annotationReplicateFrom = "%s/replicate-from"
	annotationReplicateTo   = "%s/replicate-to"
)

const (
	addOrUpdateObj action = iota
	deleteObj
)

type action int64

func (a action) String() string {
	switch a {
	case addOrUpdateObj:
		return "addOrUpdate"
	case deleteObj:
		return "delete"
	default:
		return "unknown"
	}
}

type workitem struct {
	action action
	secret *corev1.Secret
}

type Replicator interface {
	Run(ctx context.Context) error
}

type replicator struct {
	sync.Mutex
	started bool

	log *zap.Logger

	informerFactories map[string]informers.SharedInformerFactory
	informers         map[string]cache.SharedIndexInformer
	listers           map[string]listers.SecretLister

	workqueue workqueue.RateLimitingInterface
}

func RunReplicator(config *Config) error {
	r, err := NewReplicator(config)
	if err != nil {
		return err
	}
	ctx := signals.SetupSignalHandler()
	return r.Run(ctx)
}

func NewReplicator(config *Config) (Replicator, error) {
	var r replicator
	var err error

	r.log, err = createZapLogger(config.LogLevel, config.LogFormat)
	if err != nil {
		return nil, err
	}
	r.log.Info("creating secrets replicator with config", zap.Any("config", config))

	kubernetesClient, err := createKubernetesClient(config.Kubeconfig, config.Timeout)
	if err != nil {
		return nil, err
	}
	r.informerFactories = make(map[string]informers.SharedInformerFactory)
	r.informers = make(map[string]cache.SharedIndexInformer)
	r.listers = make(map[string]listers.SecretLister)
	for _, namespace := range config.Namespaces {
		informerFactory := informers.NewSharedInformerFactoryWithOptions(kubernetesClient, config.ResyncPeriod, informers.WithNamespace(namespace))
		r.informerFactories[namespace] = informerFactory
		informer := informerFactory.Core().V1().Secrets().Informer()
		r.informers[namespace] = informer
		_, err := informer.AddEventHandler(cache.ResourceEventHandlerFuncs{
			AddFunc: func(obj interface{}) {
				r.handleObject(obj, addOrUpdateObj)
			},
			UpdateFunc: func(oldObj, newObj interface{}) {
				if oldObj != newObj {
					r.handleObject(newObj, addOrUpdateObj)
				}
			},
			DeleteFunc: func(obj interface{}) {
				r.handleObject(obj, deleteObj)
			},
		})
		if err != nil {
			return nil, errors.WithMessagef(err, "error registering event handler for secrets in namespace %s", namespace)
		}
		r.listers[namespace] = informerFactory.Core().V1().Secrets().Lister()
	}

	r.workqueue = workqueue.NewRateLimitingQueueWithConfig(workqueue.DefaultControllerRateLimiter(), workqueue.RateLimitingQueueConfig{Name: "Secrets"})

	return &r, nil
}

func (r *replicator) Run(ctx context.Context) error {
	r.Lock()
	if r.started {
		r.Unlock()
		return errors.New("replicator already started")
	}
	r.started = true

	var ready bool
	defer func() {
		// Only unlock the manager if we haven't reached
		// the internal readiness condition.
		if !ready {
			r.Unlock()
		}
	}()

	defer r.log.Sync()
	defer r.workqueue.ShutDown()

	r.log.Info("starting secrets informers", zap.Any("namespaces", r.namespaces()))
	for namespace, informerFactory := range r.informerFactories {
		informerFactory.Start(ctx.Done())
		informerFactory.WaitForCacheSync(ctx.Done())
		r.log.Info("started the secrets informer", zap.String("namespace", namespace))
	}

	r.log.Info("starting worker loop")
	go r.processItems(ctx)

	r.log.Info("started the secrets replicator")
	ready = true
	r.Unlock()
	select {
	case <-ctx.Done():
		r.log.Info("shutting down the secrets replicator")
		err := context.Cause(ctx)
		if err != nil && err != context.Canceled {
			return err
		}
		return nil
	}
}

func (r *replicator) namespaces() []string {
	namespaces := make([]string, len(r.informerFactories))
	i := 0
	for namespace := range r.informerFactories {
		namespaces[i] = namespace
		i++
	}
	return namespaces
}

func (r *replicator) handleObject(obj interface{}, action action) {
	secret, err := asSecret(obj)
	if err != nil {
		r.log.Warn("failed to handle object", zap.Stringer("action", action), zap.Error(err))
		return
	}
	r.workqueue.Add(workitem{
		action: action,
		secret: secret,
	})
}

func (r *replicator) processItems(ctx context.Context) {
	for r.processNextItem(ctx) {
	}
}

func (r *replicator) processNextItem(ctx context.Context) bool {
	obj, shutdown := r.workqueue.Get()
	if shutdown {
		return false
	}

	err := func(obj interface{}) error {
		defer r.workqueue.Done(obj)
		var item workitem
		var ok bool
		if item, ok = obj.(workitem); !ok {
			r.workqueue.Forget(obj)
			return errors.Errorf("expected workitem in work queue, but got %T", obj)
		}
		r.workqueue.Forget(obj)
		r.log.Info("process secret", zap.Stringer("action", item.action), zap.String("key", item.secret.ObjectMeta.Namespace+"/"+item.secret.ObjectMeta.Name), zap.Any("secret", item.secret.ObjectMeta))
		return nil
	}(obj)

	if err != nil {
		r.log.Warn("failed to process item in work queue", zap.Error(err))
	}

	return true
}

func createZapLogger(logLevel, logFormat string) (*zap.Logger, error) {
	zapLogLevel, err := createZapLogLevel(logLevel)
	if err != nil {
		return nil, err
	}
	zapEncoding, err := createZapEncoding(logFormat)
	if err != nil {
		return nil, err
	}
	encoderCfg := zap.NewProductionEncoderConfig()
	encoderCfg.TimeKey = "timestamp"
	encoderCfg.EncodeTime = zapcore.ISO8601TimeEncoder

	config := zap.Config{
		Level:             zap.NewAtomicLevelAt(zapLogLevel),
		Development:       false,
		DisableCaller:     false,
		DisableStacktrace: false,
		Sampling:          nil,
		Encoding:          zapEncoding,
		EncoderConfig:     encoderCfg,
		OutputPaths:       []string{"stderr"},
		ErrorOutputPaths:  []string{"stderr"},
		InitialFields: map[string]interface{}{
			"pid": os.Getpid(),
		},
	}

	return config.Build()
}

func createZapLogLevel(logLevel string) (zapcore.Level, error) {
	switch strings.ToLower(logLevel) {
	case "debug":
		return zap.DebugLevel, nil
	case "info":
		return zap.InfoLevel, nil
	case "warn":
		return zap.WarnLevel, nil
	case "error":
		return zap.ErrorLevel, nil
	case "panic":
		return zap.PanicLevel, nil
	case "fatal":
		return zap.FatalLevel, nil
	default:
		return zap.InfoLevel, fmt.Errorf("'%s' is a unkown log level", logLevel)
	}
}

func createZapEncoding(logFormat string) (string, error) {
	switch strings.ToLower(logFormat) {
	case "json":
		return "json", nil
	case "plain":
		return "console", nil
	default:
		return "json", fmt.Errorf("'%s' unknown log format", logFormat)
	}
}

func createKubernetesClient(kubeconfig string, timeout time.Duration) (kubernetes.Interface, error) {
	var config *rest.Config
	var err error
	kubeconfigPath := resolveKubeconfigPath(kubeconfig)
	if kubeconfigPath != "" {
		config, err = clientcmd.NewNonInteractiveDeferredLoadingClientConfig(
			&clientcmd.ClientConfigLoadingRules{ExplicitPath: kubeconfigPath},
			&clientcmd.ConfigOverrides{Timeout: timeout.String()}).ClientConfig()
		config, err = clientcmd.BuildConfigFromFlags("", kubeconfigPath)
	} else {
		config, err = rest.InClusterConfig()
	}
	if err != nil {
		return nil, errors.WithMessage(err, "error creating kubernetes config")
	}
	return kubernetes.NewForConfig(config)
}

func resolveKubeconfigPath(kubeconfig string) string {
	if kubeconfig != "" {
		return kubeconfig
	} else if os.Getenv("KUBECONFIG") != "" {
		return os.Getenv("KUBECONFIG")
	} else if _, err := os.Stat(filepath.Join(homedir.HomeDir(), ".kube", "config")); err == nil {
		return filepath.Join(homedir.HomeDir(), ".kube", "config")
	} else {
		return ""
	}
}

func asSecret(obj interface{}) (*corev1.Secret, error) {
	var secret *corev1.Secret
	var ok bool
	if secret, ok = obj.(*corev1.Secret); !ok {
		tombstone, ok := obj.(cache.DeletedFinalStateUnknown)
		if !ok {
			return nil, errors.Errorf("error decoding object, expected secret, but got %T", obj)
		}
		secret, ok = tombstone.Obj.(*corev1.Secret)
		if !ok {
			return nil, errors.Errorf("error decoding object tombstone %s, expected secret but got %T", tombstone.Key, tombstone.Obj)
		}
	}
	return secret, nil
}
