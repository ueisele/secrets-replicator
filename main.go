package main

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"os"
	"time"
)

const (
	// The name of our config file, without the file extension because viper supports many different config file languages.
	defaultConfigFilename = "replicator"
)

var (
	configFile string
)

func main() {
	cmd := newSecretsReplicatorCommand()
	if err := cmd.Execute(); err != nil {
		os.Exit(1)
	}
}

func newSecretsReplicatorCommand() *cobra.Command {
	cmd := &cobra.Command{
		Use:     "secrets-replicator",
		Short:   "Replicator for Kubernetes Secrets",
		Long:    "Replicator for Kubernetes Secrets, following least privilege principle",
		Version: Version(),
		RunE:    runWithConfig(RunReplicator),
	}

	cmd.Flags().StringVar(&configFile, "config", "", "The configuration file for the secret replicator.")

	cmd.Flags().StringP("kubeconfig", "c", "", "The kubeconfig file to use. If not set, in-cluster authentication is used.")
	cmd.Flags().DurationP("timeout", "t", 1*time.Minute, "The connection timeout to the Kubernetes API server.")
	cmd.Flags().StringSliceP("namespaces", "n", []string{}, "Namespaces, which the replicator should observe.")
	cmd.Flags().DurationP("resyncperiod", "r", 1*time.Minute, "The period in which the resources are synced.")
	cmd.Flags().StringP("annotationprefix", "p", "replicator.v1.envite.de", "The prefix for annotations which this controller should consider.")
	cmd.Flags().StringP("statuslistener", "s", "0.0.0.0:8080", "The listener address for the status endpoint.")
	cmd.Flags().StringP("loglevel", "l", "INFO", "The log level to be used.")
	cmd.Flags().StringP("logformat", "f", "plain", "The log format ('plain' or 'json').")

	return cmd
}

func runWithConfig(configurable func(config *Config) error) func(cmd *cobra.Command, args []string) error {
	return func(cmd *cobra.Command, args []string) error {
		config, err := initializeConfig(cmd)
		if err != nil {
			return err
		}
		return configurable(config)
	}
}

func initializeConfig(cmd *cobra.Command) (*Config, error) {
	v := viper.New()

	if configFile != "" {
		v.SetConfigFile(configFile)
	} else {
		v.SetConfigName(defaultConfigFilename)
		v.AddConfigPath(".")
	}
	if err := v.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); !ok {
			return nil, err
		}
	}

	v.AutomaticEnv()

	err := v.BindPFlags(cmd.Flags())
	if err != nil {
		return nil, err
	}

	var config Config
	err = v.Unmarshal(&config)
	if err != nil {
		return nil, err
	}
	return &config, nil
}
